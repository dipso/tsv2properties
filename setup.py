from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="tsv2properties",
    version="0.0.1",
    description="Génère des fichiers properties de traduction pour Dataverse à partir de fichiers de métadonnées tsv",
    py_modules=["tsv2properties"],
    package_dir={"":"src"},
    long_description=long_description,
    long_description_content_type = "text/markdown",
    install_requires = [
        "pandas ~= 1.3.4",
        "unidecode ~= 1.3.2",
        "click~=8.0.3",
        "wheel~=0.37.1"
    ],
    extras_require = {
        "dev": [
            "pytest>=6.2.5",
            "pytest-datadir>=1.3.1"
        ],
    },
    entry_points={
        'console_scripts': [
            'tsv2properties = tsv2properties:cli',
        ],
    },
)