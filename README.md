# tsv2properties

Génération de fichiers .properties de traduction pour Dataverse à partir de fichiers de métadonnées .tsv .

## Installation

<!-- @ToDo ! + release -->
Executer
```bash
python -m pip install git+https://forgemia.inra.fr/dipso/tsv2properties
```

Vérifier l'installation en exécutant
```bash
tsv2properties --help
```

## Développement
Pour installer les paquets de test, cloner le repository puis dans un virtualenv, se placer dans tsv2properties et exécuter :
```bash
pip install -e .[dev]
```
la liste de ces paquets est indiquée dans le setup.py.

## Utilisation
Se placer dans le dossiers où sont situés les fichiers tsvs et exécuter :
```bash
tsv2properties
```
Les fichiers properties seront sauvegardés dans le dossier actuel.

Ou utiliser les arguments en exécutant :
```bash
tsv2properties --dir={chemin des tsvs} --out={chemin des properties}
```