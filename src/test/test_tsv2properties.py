from cmath import nan
import tsv2properties as t2p
import pandas as pd

# Test Data

test_fields = pd.DataFrame(
    {'name':['simpleField', 'controlledField'],
    'title':['Simple Field', 'Controlled Field'],
    'description': ['Description simpleField', 'Description controlledField']
    })

test_vocabularies = pd.DataFrame(
    {'DatasetField':['controlledField', 'controlledField', 'controlledField'],
    'Value':['Champ avec id', 'Colonnes supplémentaires', 'ÅÉ Ô éèà'],
    'identifier':['id', nan, nan]
    })
test_vocabularies.index = [3,4,5] # harmonisation avec l'index extrait du fichier test

# Tests

def test_tsvDivider(shared_datadir):
    data_frames = t2p.tsvDivider(path_to_tsv = shared_datadir / 'test.tsv')
    fields = data_frames[0]
    vocabularies = data_frames[1]
    assert fields.equals(test_fields)
    assert vocabularies.equals(test_vocabularies)

def test_extractDatasetFields(shared_datadir):
    fields_properties_path = shared_datadir / 'output_fields.properties'
    t2p.extractDatasetFields(dataset_fields = test_fields, output_file_path = fields_properties_path)
    with open(shared_datadir / 'test_fields.properties') as test_fields_properties:
        with open(fields_properties_path, 'r') as properties:
            assert properties.read() == test_fields_properties.read()

def test_convertControlledVocabularies(shared_datadir):
    vocabularies_properties_path = shared_datadir / "output_vocabularies.properties"
    t2p.convertControlledVocabularies(controlled_vocabularies = test_vocabularies, output_file_path = vocabularies_properties_path)
    with open(shared_datadir / "test_vocabularies.properties") as test_vocabularies_properties:
        with open(vocabularies_properties_path, 'r') as properties:
            assert properties.read() == test_vocabularies_properties.read()

def test_singleTsvToProperties(shared_datadir):
    t2p.singleTsvToProperties(path_to_tsv = shared_datadir / 'test.tsv', path_to_properties = shared_datadir / 'output_test.properties')
    assert 1 != 1

def test_tsvsToProperties(shared_datadir):
    t2p.tsvsToProperties(tsv_dir = shared_datadir, properties_dir = shared_datadir)
    assert 1 != 1
