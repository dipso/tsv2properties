import pandas as pd
import os
from typing import Tuple
from pandas.core.frame import DataFrame
from unidecode import unidecode
import click

@click.command()
@click.version_option()
@click.option('--dir', default=os.getcwd(), help='Répertoire des fichiers tsv à utiliser.')
@click.option('--out', default=os.getcwd(), help='Répertoire où exporter les fichiers properties.')
def cli(dir:str, out:str):
    """Génération de fichiers .properties de traduction pour Dataverse à partir de fichiers de métadonnées .tsv ."""
    if not out.endswith("/"):
        out+="/"
    tsvsToProperties(dir,out)
    click.echo(f"tsv de {dir} convertis dans {out}")

def tsvDivider(path_to_tsv: str) -> Tuple[DataFrame, DataFrame]:
    """Importe le fichier tsv et le divise en deux DataFrames pour récupérer les informations des champs de métadonnées et des valeurs contôlées.

    Args:
        path_to_tsv: Chemin vers le fichier tsv à utiliser.

    Returns:
        dataset_fields: DataFrame contenant les données du tsv sur les champs de métadonnées.
        controlled_vocabularies: DataFrame contenant les données du tsv sur les valeurs contrôlées.
    """
    tsv = pd.read_csv(path_to_tsv, skiprows=2, sep="\t", usecols=["#datasetField", "name", "title", "description"])
    # Récupération du numéro de ligne séparant champs et valeurs contrôlées
    splitting_row_number=tsv[tsv["#datasetField"] == "#controlledVocabulary"].index[0]
    # La colonne est supprimée pour ne pas diffuser dans les DataFrames suivants
    tsv.drop("#datasetField",axis=1,inplace=True)
    # Création d'un DataFrame pour les champs de métadonnées
    dataset_fields=tsv.head(splitting_row_number)
    # Création d'un DataFrame pour les valeurs de métadonnées contrôlées
    controlled_vocabularies=tsv.tail(len(tsv)-(splitting_row_number+1))
    # Renommage des colonnes pour les valeurs de métadonnées contrôlées
    controlled_vocabularies.set_axis(list(tsv.loc[splitting_row_number]),axis='columns', inplace=True)
    return dataset_fields,controlled_vocabularies

def extractDatasetFields(dataset_fields: DataFrame, output_file_path: str) -> None:
    """Alimente les lignes "title" et "description" du fichier properties avec les valeurs du tsv chargé.

    Args:
        dataset_fields: DataFrame contenant les données du tsv sur les champs de métadonnées.
        output_file_path: Chemin vers le fichier properties à alimenter.
    """
    dataset_fields = dataset_fields.fillna("")
    # Ouverture avec 'w' pour écraser le contenu
    with open(output_file_path, 'w') as properties:
        # Alimentation des lignes .title contenant le libellé du champ de métadonnée
        titles = (
            "datasetfieldtype." + dataset_fields.loc[:,"name"] +
            "." + "title" +
            "=" + dataset_fields.loc[:,"title"]
        )
        properties.write("\n".join(map(str, titles.to_list())))
        # saut de ligne entre titles et descriptions
        properties.write("\n")
        # Alimentation des lignes .description contenant la description (infobulle) du champ de métadonnée
        descriptions = (
           "datasetfieldtype." + dataset_fields.loc[:,"name"] +
            "." + "description" +
            "=" + dataset_fields.loc[:,"description"].fillna("") # fillna("") pour les champs sans description (ex. blocs)
        )
        properties.write("\n".join(map(str, descriptions.to_list())))

def convertControlledVocabularies(controlled_vocabularies: DataFrame, output_file_path: str) -> None:
    """Alimente les lignes "controlledvocabulary" du fichier properties avec les valeurs du tsv chargé.

    Args:
        controlled_vocabularies: DataFrame contenant les données du tsv sur les valeurs contrôlées.
        output_file_path: Chemin vers le fichier properties à alimenter.
    """
    # Ouverture avec 'a' pour ne pas écraser les lignes sur les champs
    with open(output_file_path, 'a') as properties:
        # Récupération des identifiants ou si absent de la valeur adaptée
        identifiers = [unidecode(x).lower().replace(" ","_") for x in controlled_vocabularies.loc[:,"identifier"].fillna(controlled_vocabularies.loc[:,"Value"])]
        values = (
            "controlledvocabulary." + controlled_vocabularies.loc[:,"DatasetField"] +
            "." + identifiers +
            "=" + controlled_vocabularies.loc[:,"Value"]
        )
        properties.write("\n".join(map(str, values.to_list())))

def singleTsvToProperties(path_to_tsv: str, path_to_properties: str) -> None:
    """Génère un fichier properties à partir d'un fichier tsv.

    Args:
        path_to_tsv: Chemin vers le fichier tsv à utiliser.
        path_to_properties: Chemin vers le fichier properties à alimenter.
    """
    dataset_fields,controlled_vocabularies = tsvDivider(path_to_tsv)
    extractDatasetFields(dataset_fields, path_to_properties)
    convertControlledVocabularies(controlled_vocabularies, path_to_properties)

def tsvsToProperties(tsv_dir: str, properties_dir: str)-> None:
    """Génère les fichiers properties correspondant pour tout les tsvs d'un répertoire

    Args:
        tsv_dir: Chemin vers le répertoire contentant les tsvs.
        properties_dir: Chemin vers le répertoire où générer les properties.
    """
    for file in os.listdir(tsv_dir):
        if file.endswith(".tsv"):
            singleTsvToProperties(tsv_dir+file, properties_dir+os.path.splitext(file)[0]+".properties")